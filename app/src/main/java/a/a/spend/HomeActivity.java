package a.a.spend;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TabHost;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;


public class HomeActivity extends ActionBarActivity {

    Toolbar toolbar;
    public static String facebookId;
    TabHost mTabHost;

    private PendingIntent pendingIntent;
    private AlarmManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (ParseUser.getCurrentUser() == null) {
            ParseFacebookUtils.logIn(this, new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException error) {
                    // When your user logs in, immediately get and store its Facebook ID
                    if (user != null) {
                        getFacebookIdInBackground();
                        Spend.updateParseInstallation(user);

                        Intent alarmIntent = new Intent(HomeActivity.this, AlarmReceiver.class);
                        pendingIntent = PendingIntent.getBroadcast(HomeActivity.this, 0, alarmIntent, 0);
                        AlarmReceiver alarmReceiver = new AlarmReceiver();
                        alarmReceiver.SetAlarm(HomeActivity.this);

                    } else if (user == null) {
                        ParseFacebookUtils.logIn(HomeActivity.this, new LogInCallback() {
                            @Override
                            public void done(ParseUser parseUser, ParseException e) {
                                if (parseUser != null) {
                                    getFacebookIdInBackground();
                                }
                            }
                        });
                    }
                }
            });
        }
        else{
            getFacebookId();
        }

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        NavigationDrawerFragment navigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer_fragment);
        navigationDrawerFragment.setup(R.id.navigation_drawer_fragment, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();

        getTab("SUMMARY", "SUMMARY", R.id.SUMMARY);
        getTab("STATS", "STATS", R.id.STATS);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
    }

    private void getFacebookIdInBackground() {
        Request.executeMeRequestAsync(ParseFacebookUtils.getSession(), new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                if (user != null) {
                    SharedPreferences preferences = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("facebook_Id", user.getId());
                    editor.commit();
                    facebookId = preferences.getString("facebook_Id", null);
                    Log.i("facebook", "facebookId: " + facebookId);

                    ParseUser.getCurrentUser().put("fbId", user.getId());
                    ParseUser.getCurrentUser().saveInBackground();
                }
            }
        });
    }

    public void getTab(String tag, String tabIndicator, int layoutId) {
        TabHost.TabSpec tabSpec = mTabHost.newTabSpec(tag);

        tabSpec.setIndicator(tabIndicator);
        tabSpec.setContent(layoutId);
        mTabHost.addTab(tabSpec);
    }

    public void getFacebookId(){
        SharedPreferences preferences = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        facebookId = preferences.getString("facebook_Id", null);
        Log.i("facebook", "facebookId: " + facebookId);
    }
}
