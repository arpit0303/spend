package a.a.spend;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Arpit on 24/03/15.
 */
public class StatsFragment extends Fragment {

    ParseQuery<ParseObject> manualQuery;
    static int travelSpend = 0;
    static int foodSpend = 0;
    static int entertainmentSpend = 0;
    static int shoppingSpend = 0;
    static int billsSpend = 0;
    static int othersSpend = 0;

    TextView mTravel;
    TextView mFood;
    TextView mEntertainment;
    TextView mShopping;
    TextView mBills;
    TextView mOthers;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stats, container, false);

        mTravel = (TextView) rootView.findViewById(R.id.categoryTravelSpend);
        mFood = (TextView) rootView.findViewById(R.id.categoryFoodSpend);
        mEntertainment = (TextView) rootView.findViewById(R.id.categoryEntertainmentSpend);
        mShopping = (TextView) rootView.findViewById(R.id.categoryShoppingSpend);
        mBills = (TextView) rootView.findViewById(R.id.categoryBillsSpend);
        mOthers = (TextView) rootView.findViewById(R.id.categoryOthersSpend);

        calculateStats();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        manualQuery.clearCachedResult();
        manualQuery.cancel();
        calculateStats();
    }

    private void calculateStats() {
        manualQuery = ParseQuery.getQuery(Constants.PARSE_MANUAL_DATA + HomeActivity.facebookId);

        manualQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                travelSpend = 0;
                foodSpend = 0;
                entertainmentSpend = 0;
                shoppingSpend = 0;
                billsSpend = 0;
                othersSpend = 0;

                for (ParseObject manual : parseObjects) {
                    String mCategory = manual.getString(Constants.PARSE_CATEGORY);

                    if (mCategory.equals(Constants.PARSE_CATEGORY_TRAVEL)) {
                        travelSpend += (int) manual.getNumber(Constants.PARSE_MANUAL_SPEND);
                    }
                    if (mCategory.equals(Constants.CATEGORY_FOOD_AND_DRINK)) {
                        foodSpend += (int) manual.getNumber(Constants.PARSE_MANUAL_SPEND);
                    }
                    if (mCategory.equals(Constants.PARSE_CATEGORY_ENTERTAINMENT)) {
                        entertainmentSpend += (int) manual.getNumber(Constants.PARSE_MANUAL_SPEND);
                    }
                    if (mCategory.equals(Constants.PARSE_CATEGORY_SHOPPING)) {
                        shoppingSpend += (int) manual.getNumber(Constants.PARSE_MANUAL_SPEND);
                    }
                    if (mCategory.equals(Constants.PARSE_CATEGORY_BILLS)) {
                        billsSpend += (int) manual.getNumber(Constants.PARSE_MANUAL_SPEND);
                    }
                    if (mCategory.equals(Constants.PARSE_CATEGORY_OTHERS)) {
                        othersSpend += (int) manual.getNumber(Constants.PARSE_MANUAL_SPEND);
                    }
                }

                mTravel.setText(travelSpend + "");
                mFood.setText(foodSpend + "");
                mEntertainment.setText(entertainmentSpend + "");
                mShopping.setText(shoppingSpend + "");
                mBills.setText(billsSpend + "");
                mOthers.setText(othersSpend + "");
            }
        });
    }
}
