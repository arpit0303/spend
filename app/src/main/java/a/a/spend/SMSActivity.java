package a.a.spend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class SMSActivity extends ActionBarActivity {

    Toolbar toolbar;
    ListView smsList;
    List<String> smsAddress;
    List<String> smsBody;
    List<Date> smsReceiveDate;
    List<Integer> smsPrice;
    String[][] mKeywords = {{" rs."}};
    Date mLatestDate;
    Cursor cursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        NavigationDrawerFragment navigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer_fragment_sms);
        navigationDrawerFragment.setup(R.id.navigation_drawer_fragment_sms, (DrawerLayout) findViewById(R.id.drawer_layout_sms), toolbar);

        smsList = (ListView) findViewById(R.id.smsListView);
        smsAddress = new ArrayList<String>();
        smsBody = new ArrayList<String>();
        smsReceiveDate = new ArrayList<Date>();
        smsPrice = new ArrayList<Integer>();

        if (checkNetworkStatus()) {
            getFilterAndStoreSMS();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(SMSActivity.this);
            builder.setTitle("No Network!!")
                    .setMessage("There is no internet connection.")
                    .setPositiveButton(android.R.string.ok, null);

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private boolean checkNetworkStatus() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        Boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }
        return isAvailable;
    }

    private void getFilterAndStoreSMS() {
        //getting all the SMS
        cursor = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);

        final ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PARSE_SMS_CLASS + HomeActivity.facebookId);
        try {
            if (query.count() > 0) {
                query.addDescendingOrder(Constants.PARSE_RECEIVED_DATE);
                ParseObject lastDate = query.getFirst();
                mLatestDate = lastDate.getDate(Constants.PARSE_RECEIVED_DATE);
                Log.i("SMS time", "Time: " + mLatestDate.toString());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < mKeywords.length; i++) {
            if (cursor.moveToFirst()) { // must check the result to prevent exception
                do {
                    int flag = 0;
                    String msgData = (cursor.getString((cursor.getColumnIndexOrThrow("body")))).toLowerCase();

                    //Filtering all the sms according to the pair of Keywords
                    for (int k = 0; k < mKeywords[i].length; k++) {
                        if (msgData.contains(mKeywords[i][k])) {
                            flag = k + 1;
                        } else {
                            break;
                        }
                    }

                    //Checking One pair of keywords exist in particular message if yes then push it into List.
                    if (flag == mKeywords[i].length) {
                        String ms = cursor.getString(cursor.getColumnIndexOrThrow("date"));
                        Date smsDayTime = new Date(Long.valueOf(ms));

                        if (mLatestDate == null) {
                            smsReceiveDate.add(smsDayTime);
                            smsAddress.add(cursor.getString(cursor.getColumnIndexOrThrow("address")));
                            smsBody.add(cursor.getString(cursor.getColumnIndexOrThrow("body")));
                        }
                        if (mLatestDate != null) {
                            if (smsDayTime.after(mLatestDate)) {
                                smsReceiveDate.add(smsDayTime);
                                smsAddress.add(cursor.getString(cursor.getColumnIndexOrThrow("address")));
                                smsBody.add(cursor.getString(cursor.getColumnIndexOrThrow("body")));
                            }
                        }
                    }
                } while (cursor.moveToNext());
            } else {
                // empty box, no SMS
                AlertDialog.Builder builder = new AlertDialog.Builder(SMSActivity.this);
                builder.setTitle("Oops!!!")
                        .setMessage("No Spends till now.")
                        .setPositiveButton(android.R.string.ok, null);

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        }

        //Converting All the list to Array
        if (smsReceiveDate.size() > 0) {

            String[] parseSMSAddress = new String[smsAddress.size()];
            parseSMSAddress = smsAddress.toArray(parseSMSAddress);

            String[] parseSMSBody = new String[smsBody.size()];
            parseSMSBody = smsBody.toArray(parseSMSBody);

            Date[] parseSMSReceivedDate = new Date[smsReceiveDate.size()];
            parseSMSReceivedDate = smsReceiveDate.toArray(parseSMSReceivedDate);

            //getting sms inside Price
            getSMSPrice(parseSMSBody);

            Integer[] parseSMSPrice = new Integer[smsPrice.size()];
            parseSMSPrice = smsPrice.toArray(parseSMSPrice);

            //Pushing Filtered SMS into Parse One by One
            for (int i = 0; i < parseSMSAddress.length; i++) {
                storeInParse(parseSMSAddress[i], parseSMSBody[i], parseSMSReceivedDate[i], parseSMSPrice[i]);
            }
        }
        getDataFromParse();
    }

    private void getDataFromParse() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PARSE_SMS_CLASS + HomeActivity.facebookId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    //success
                    List<String> SMSAddress = new ArrayList<String>();
                    List<String> SMSBody = new ArrayList<String>();

                    for (ParseObject parseObject : parseObjects) {
                        SMSAddress.add(parseObject.getString(Constants.PARSE_ADDRESS));
                        SMSBody.add(parseObject.getString(Constants.PARSE_BODY));
                    }
                    SMSAdapter adapter = new SMSAdapter(SMSActivity.this, SMSAddress, SMSBody);
                    smsList.setAdapter(adapter);
                } else {
                    //error
                    AlertDialog.Builder builder = new AlertDialog.Builder(SMSActivity.this);
                    builder.setMessage(getString(R.string.save_message))
                            .setTitle(getString(R.string.save_title))
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
    }

    private void storeInParse(String pSMSAddress, String pSMSBody, Date pSMSRDate, Integer pSMSPrice) {
        ParseObject mSMS = new ParseObject(Constants.PARSE_SMS_CLASS + HomeActivity.facebookId);

        mSMS.put(Constants.PARSE_ADDRESS, pSMSAddress);
        mSMS.put(Constants.PARSE_BODY, pSMSBody);
        mSMS.put(Constants.PARSE_RECEIVED_DATE, pSMSRDate);
        mSMS.put(Constants.PARSE_SMS_INSIDE_PRICE, pSMSPrice);

        mSMS.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    //success
                    System.out.println("Message sent");
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SMSActivity.this);
                    builder.setMessage(getString(R.string.save_message))
                            .setTitle(getString(R.string.save_title))
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });

        //For Monthly Spend
        ParseObject mMonthlySpend = new ParseObject(Constants.PARSE_MONTHLY_SPEND + HomeActivity.facebookId);

        mMonthlySpend.put(Constants.PARSE_MONTH, pSMSRDate.getMonth());
        mMonthlySpend.put(Constants.PARSE_TOTAL_MONTHLY_SPEND, pSMSPrice);

        mMonthlySpend.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    //success
                    System.out.println("Message sent");
                } else {
                    //error
                    AlertDialog.Builder builder = new AlertDialog.Builder(SMSActivity.this);
                    builder.setMessage(getString(R.string.save_message))
                            .setTitle(getString(R.string.save_title))
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(SMSActivity.this, HomeActivity.class));
        finish();
        super.onBackPressed();
        cursor.close();
    }

    public void getSMSPrice(String[] allSMSData) {
        //Taking SMS one by one -> get the index of " rs." -> create a subString after the keyword -> remove space if it is there in first place
        //->after removing front space character get the index of first space -> Again subString  to the subString from 0 to the SubIndex
        // -> Convert String into float and then covert into int
        for (int i = 0; i < allSMSData.length; i++) {
            int smsIndex = allSMSData[i].toLowerCase().indexOf(" rs.");

            if (smsIndex != -1) {
                String subSMS = allSMSData[i].substring(smsIndex + 4);
                if (subSMS.charAt(0) == ' ') {
                    subSMS = subSMS.substring(1);
                }
                int subSMSIndex = subSMS.indexOf(" ");
                String price = subSMS.substring(0, subSMSIndex);
                Log.i("hello",price);
                price = price.replaceAll("[^0-9]","");
                int roundPrice = (int) Math.round(Float.parseFloat(price));
                //Adding all the prices to list
                smsPrice.add(roundPrice);
            }
        }
    }
}
