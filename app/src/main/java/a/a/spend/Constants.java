package a.a.spend;

/**
 * Created by Arpit on 17/03/15.
 */
public class Constants {

    public static final String PARSE_USER_ID = "userID";
    //SMS Parse class element
    public static final String PARSE_SMS_CLASS = "SMS";
    public static final String PARSE_ADDRESS = "Address";
    public static final String PARSE_BODY = "SMSBody";
    public static final String PARSE_RECEIVED_DATE = "SMS_Received_Date";
    public static final String PARSE_SMS_INSIDE_PRICE = "SMSInsidePrice";

    //ManualData Parse class element
    public static final String PARSE_MANUAL_DATA = "ManualData";
    public static final String PARSE_CATEGORY = "Category";
    public static final String PARSE_MANUAL_SPEND = "ManualSpend";
    public static final String PARSE_MANUAL_DATE = "ManualDate";
    public static final String PARSE_CREATED_AT = "createdAt";

    //MonthlySpend Parse class element
    public static final String PARSE_MONTHLY_SPEND = "MonthlySpend";
    public static final String PARSE_MONTH = "Month";
    public static final String PARSE_TOTAL_MONTHLY_SPEND = "MONTHLY_SPEND";

    //category
    public static final String PARSE_CATEGORY_TRAVEL = "Travel";
    public static final String CATEGORY_FOOD_AND_DRINK = "Food & Drink";
    public static final String PARSE_CATEGORY_FOOD_AND_DRINK = "Food_And_Drink";
    public static final String PARSE_CATEGORY_ENTERTAINMENT = "Entertainment";
    public static final String PARSE_CATEGORY_SHOPPING = "Shopping";
    public static final String PARSE_CATEGORY_BILLS = "Bills";
    public static final String PARSE_CATEGORY_OTHERS = "Others";
}
