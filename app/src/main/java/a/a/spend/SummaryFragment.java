package a.a.spend;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Arpit on 24/03/15.
 */
public class SummaryFragment extends Fragment implements View.OnClickListener {

    String[] mMonth = {"JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER",
            "NOVEMBER", "DECEMBER"};
    Calendar mCalendar = Calendar.getInstance();
    static int index = 0;
    TextView monthText;
    public static int totalSpend = 0;
    TextView mTotalSpendText;
    ParseQuery<ParseObject> monthlyQuery;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_summary, container, false);

        monthText = (TextView) rootView.findViewById(R.id.month_text);
        ImageButton previousBtn = (ImageButton) rootView.findViewById(R.id.previousButton);
        ImageButton nextBtn = (ImageButton) rootView.findViewById(R.id.nextButton);
        mTotalSpendText = (TextView) rootView.findViewById(R.id.totalSpend);

        index = mCalendar.get(Calendar.MONTH);

        monthText.setText(mMonth[index]);
        previousBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        calculateMonthlySpend(index);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        monthlyQuery.clearCachedResult();
        monthlyQuery.cancel();
        calculateMonthlySpend(index);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.previousButton:
                monthlyQuery.clearCachedResult();
                monthlyQuery.cancel();
                index--;
                if (index < 0) {
                    index = 11;
                }
                calculateMonthlySpend(index);
                monthText.setText(mMonth[index]);
                break;
            case R.id.nextButton:
                monthlyQuery.clearCachedResult();
                monthlyQuery.cancel();
                index++;
                if (index > 11) {
                    index = 0;
                }
                calculateMonthlySpend(index);
                monthText.setText(mMonth[index]);
                break;
            default:
                break;
        }
    }

    private void calculateMonthlySpend(int monthIndex) {
        totalSpend = 0;

        monthlyQuery = ParseQuery.getQuery(Constants.PARSE_MONTHLY_SPEND + HomeActivity.facebookId);

        try {
            List<ParseObject> monthlySpend = monthlyQuery.find();
            if (monthlySpend.size() > 0) {
                for (ParseObject monthly : monthlySpend) {
                    int monthlyMonth = (int) monthly.getNumber(Constants.PARSE_MONTH);
                    if (monthlyMonth == monthIndex) {
                        totalSpend += (int) monthly.getNumber(Constants.PARSE_TOTAL_MONTHLY_SPEND);
                    }
                }
                mTotalSpendText.setText(totalSpend + "");
            }

            if (monthlySpend.size() <= 0) {
                mTotalSpendText.setText(0 + "");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
