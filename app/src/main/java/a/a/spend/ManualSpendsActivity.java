package a.a.spend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;


public class ManualSpendsActivity extends ActionBarActivity {

    protected List<ParseObject> mSpend;
    protected SwipeRefreshLayout mSwipeRefreshLayout;
    Toolbar toolbar;
    ListView mManualList;
    List<String> mManualCategory;
    List<Integer> mManualSpend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_spends);

        mManualCategory = new ArrayList<String>();
        mManualSpend = new ArrayList<Integer>();

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        mManualList = (ListView) findViewById(R.id.manual_list);

        NavigationDrawerFragment navigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer_fragment_manual);
        navigationDrawerFragment.setup(R.id.navigation_drawer_fragment_manual, (DrawerLayout) findViewById(R.id.drawer_layout_manual), toolbar);

        final ManualDialogFragment manualDialogFragment = new ManualDialogFragment();

        ImageButton mManualAddButton = (ImageButton) findViewById(R.id.manual_add_button);
        mManualAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manualDialogFragment.show(getSupportFragmentManager(), ManualDialogFragment.TAG);
                if(checkNetworkStatus()){
                    getManualDataFromParse();
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(ManualSpendsActivity.this);
                    builder.setTitle("No Network!!")
                            .setMessage("There is no internet connection.")
                            .setPositiveButton(android.R.string.ok, null);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                if(checkNetworkStatus()){
                    getManualDataFromParse();
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(ManualSpendsActivity.this);
                    builder.setTitle("No Network!!")
                            .setMessage("There is no internet connection.")
                            .setPositiveButton(android.R.string.ok, null);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.swipeRefresh1, R.color.swipeRefresh2,
                R.color.swipeRefresh3, R.color.swipeRefresh4);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getManualDataFromParse();
    }

    private boolean checkNetworkStatus() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        Boolean isAvailable = false;
        if(networkInfo != null && networkInfo.isConnected()){
            isAvailable = true;
        }
        return isAvailable;
    }

    public void getManualDataFromParse() {
        mManualCategory.clear();
        mManualSpend.clear();
        mManualList.removeAllViewsInLayout();

        ParseQuery<ParseObject> query = ParseQuery.getQuery(Constants.PARSE_MANUAL_DATA + HomeActivity.facebookId);
        if (query != null) {
            query.orderByDescending(Constants.PARSE_CREATED_AT);

            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> parseObjects, ParseException e) {
                    if (mSwipeRefreshLayout.isRefreshing()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }

                    if (e == null) {
                        mSpend = parseObjects;

                        for (ParseObject p : mSpend) {
                            mManualCategory.add(p.getString(Constants.PARSE_CATEGORY));
                            mManualSpend.add(p.getInt(Constants.PARSE_MANUAL_SPEND));
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ManualSpendAdapter adapter = new ManualSpendAdapter(ManualSpendsActivity.this, mManualCategory, mManualSpend, mSpend);
                                mManualList.setAdapter(adapter);
                            }
                        });

                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ManualSpendsActivity.this, HomeActivity.class));
        finish();
    }
}
