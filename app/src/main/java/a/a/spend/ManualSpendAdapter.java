package a.a.spend;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.Date;
import java.util.List;

/**
 * Created by Arpit on 18/03/15.
 */
public class ManualSpendAdapter extends ArrayAdapter<String>{

    Context mContext;
    List<String> mCategory;
    List<Integer> mSpend;
    List<ParseObject> mMsg;


    public ManualSpendAdapter(Context context, List<String> category, List<Integer> spend, List<ParseObject> msg) {
        super(context, R.layout.list_item_manual_spend, category);
        mContext = context;
        mCategory = category;
        mSpend = spend;
        mMsg = msg;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item_manual_spend, null);
            holder = new ViewHolder();
            holder.categoryText = (TextView)convertView.findViewById(R.id.manual_category);
            holder.spendText = (TextView)convertView.findViewById(R.id.manual_spend);
            holder.timeSpan = (TextView) convertView.findViewById(R.id.timeSpan);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        holder.categoryText.setText(mCategory.get(position));

        holder.spendText.setText(mSpend.get(position) + "");

        ParseObject message = mMsg.get(position);

        Date createdAt = message.getCreatedAt();
        long now = new Date().getTime();
        String convertedDate = DateUtils.getRelativeTimeSpanString(
                createdAt.getTime(),
                now,
                DateUtils.SECOND_IN_MILLIS).toString();

        holder.timeSpan.setText(convertedDate);
        return convertView;
    }

    public class ViewHolder{
        TextView categoryText;
        TextView spendText;
        TextView timeSpan;
    }
}
