package a.a.spend;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Arpit on 18/03/15.
 */
public class ManualDialogFragment extends android.support.v4.app.DialogFragment{

    public static final String TAG = ManualDialogFragment.class.getSimpleName().toString();
    String[] categoryList = {"Choose Category", Constants.PARSE_CATEGORY_TRAVEL, Constants.CATEGORY_FOOD_AND_DRINK,
                            Constants.PARSE_CATEGORY_ENTERTAINMENT, Constants.PARSE_CATEGORY_SHOPPING, Constants.PARSE_CATEGORY_BILLS, Constants.PARSE_CATEGORY_OTHERS};
    int selectedCategoryPosition = 0;
    String category;
    int categorySpend;

    Calendar mCalender = Calendar.getInstance();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.manual_dialog, container, false);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        Spinner mCategory = (Spinner) rootView.findViewById(R.id.category_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, categoryList);
        mCategory.setAdapter(adapter);

        mCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCategoryPosition = position;
                category = (String) parent.getItemAtPosition(selectedCategoryPosition);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final EditText mSpend = (EditText) rootView.findViewById(R.id.spend_edit);

        Button submitButton = (Button) rootView.findViewById(R.id.dialog_submit);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categorySpend = Integer.parseInt(mSpend.getText().toString());

                if(selectedCategoryPosition > 0 && categorySpend > 0){

                    Date mDate = mCalender.getTime();

                    ParseObject mManualData = new ParseObject(Constants.PARSE_MANUAL_DATA + HomeActivity.facebookId);

                    mManualData.put(Constants.PARSE_CATEGORY, category);
                    mManualData.put(Constants.PARSE_MANUAL_SPEND, categorySpend);
                    mManualData.put(Constants.PARSE_MANUAL_DATE, mDate);

                    mManualData.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                //success
                                System.out.println("Message sent");
                            } else {
                                //error
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(getString(R.string.save_message))
                                        .setTitle(getString(R.string.save_title))
                                        .setPositiveButton(android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });

                    //For Monthly Spend
                    ParseObject mMonthlySpend = new ParseObject(Constants.PARSE_MONTHLY_SPEND + HomeActivity.facebookId);

                    mMonthlySpend.put(Constants.PARSE_MONTH, mDate.getMonth());
                    mMonthlySpend.put(Constants.PARSE_TOTAL_MONTHLY_SPEND, categorySpend);

                    mMonthlySpend.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                //success
                                System.out.println("Message sent");
                            } else {
                                //error
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(getString(R.string.save_message))
                                        .setTitle(getString(R.string.save_title))
                                        .setPositiveButton(android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });

                    getDialog().dismiss();
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Oops!").setMessage("Missing Fields. Please Fill all the Fields.")
                            .setPositiveButton(android.R.string.ok, null);

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
