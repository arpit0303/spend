package a.a.spend;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Arpit on 26/03/15.
 */
public class AlarmReceiver extends BroadcastReceiver {

    public static int totalSpend = 0;
    ParseQuery<ParseObject> monthlyQuery;
    Calendar mCalender = Calendar.getInstance();

    @Override
    public void onReceive(Context context, Intent intent) {
        // For our recurring task, we'll just display a message
        calculateMonthlySpend(mCalender.get(Calendar.MONTH));

        ParsePush push = new ParsePush();
        push.setMessage("Your this month spend till now is " + totalSpend);
        push.setChannel(ParseUser.getCurrentUser().getObjectId());
        push.sendInBackground();
    }

    public static void SetAlarm(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 60 * 24 * 1, pi); // Millisec * Second * Minute * hour * days
    }

    public void CancelAlarm(Context context) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    private void calculateMonthlySpend(int monthIndex) {
        totalSpend = 0;

        monthlyQuery = new ParseQuery<ParseObject>(Constants.PARSE_MONTHLY_SPEND + HomeActivity.facebookId);

        monthlyQuery.clearCachedResult();

        try {
            List<ParseObject> monthlySpend = monthlyQuery.find();
            if (monthlySpend.size() > 0) {
                for (ParseObject monthly : monthlySpend) {
                    int monthlyMonth = (int) monthly.getNumber(Constants.PARSE_MONTH);
                    if (monthlyMonth == monthIndex) {
                        totalSpend += (int) monthly.getNumber(Constants.PARSE_TOTAL_MONTHLY_SPEND);
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
