package a.a.spend;

import android.app.Application;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by Arpit on 16/03/15.
 */
public class Spend extends Application {

    String[] mMonth = {"JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER",
            "NOVEMBER", "DECEMBER"};

    @Override
    public void onCreate() {
        super.onCreate();
        //Parse.enableLocalDatastore(this);

        Parse.initialize(this, "kzsPTAYdr4yaagI9QULwaj1c36tUVz5V3b50ou09", "bJLDjHD96p0UHX7KWlorzC51crYY3XIqnjNHsmTg");

        // Save the current Installation to Parse.
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }

    public static void updateParseInstallation(ParseUser user) {
        ParsePush.subscribeInBackground(user.getObjectId(), new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });
    }
}
