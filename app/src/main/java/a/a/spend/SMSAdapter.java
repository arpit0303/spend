package a.a.spend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Arpit on 15/03/15.
 */
public class SMSAdapter extends ArrayAdapter<String> {
    protected Context mContext;
    protected List<String> sAddress,sSmsBody;

    public SMSAdapter(Context context, List<String> address, List<String> body) {
        super(context, R.layout.list_item_sms, address);
        mContext = context;
        sAddress = address;
        sSmsBody = body;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item_sms, null);
            holder = new ViewHolder();
            holder.mAddress = (TextView)convertView.findViewById(R.id.addressText);
            holder.mBody = (TextView)convertView.findViewById(R.id.smsBodyText);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        holder.mAddress.setText(sAddress.get(position));

        holder.mBody.setText(sSmsBody.get(position));
        return convertView;
    }

    private static class ViewHolder{
        TextView mAddress;
        TextView mBody;

    }

}
